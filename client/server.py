from celery import Celery

app = Celery('tasks', broker='amqp://guest@localhost//')

@app.task
def add(text):
    return text + " recived"
