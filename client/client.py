#from tasks import add
from celery import Celery


class Test_Server():
    string_to_send = "hallo World"

    def run(self):
        celery = Celery('broker_test', broker='amqp://guest@localhost//')
        print(celery.send_task('importer.tasks.add_drive_data', (111,111)))
        #send some string to queue


test = Test_Server();
test.run()
